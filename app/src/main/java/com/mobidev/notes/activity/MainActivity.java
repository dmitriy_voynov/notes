package com.mobidev.notes.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.mobidev.notes.R;
import com.mobidev.notes.adapter.NotesAdapter;
import com.mobidev.notes.fragment.ListFragment;
import com.mobidev.notes.model.Annotation;
import com.mobidev.notes.model.Note;

public class MainActivity extends Activity {
	public final static String EXTRA_MESSAGE = "com.mobidev.notes.MESSAGE";
	public final static String EXTRA_TYPE = "com.mobidev.notes.TYPE";
	public final static String EXTRA_ID = "com.mobidev.notes.ID";

	public final static String NOTES_STATE = "com.mobidev.NOTES";

	final static int NOTE_CREATE_REQUEST = 1;
	final static int NOTE_EDIT_REQUEST = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);

		ListFragment list = (ListFragment) getFragmentManager().findFragmentById(R.id.f_list);

		final MainActivity instance = this;
		list.setCallbacks(new ListFragment.ListCallbacks() {
			@Override
			public void onListItemClick(Annotation note, int id) {
				Intent detailIntent = new Intent(instance, DetailsActivity.class);
				detailIntent.putExtra(EXTRA_MESSAGE, note.toString());
				detailIntent.putExtra(EXTRA_TYPE, note.typeToString());
				detailIntent.putExtra(EXTRA_ID, id);
				startActivityForResult(detailIntent, NOTE_EDIT_REQUEST);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if ((data != null) && (resultCode == RESULT_OK)
				&& ((requestCode == NOTE_EDIT_REQUEST) || (requestCode == NOTE_CREATE_REQUEST))) {
			String message = data.getStringExtra(MainActivity.EXTRA_MESSAGE);
			String typeString = data.getStringExtra(MainActivity.EXTRA_TYPE);
			Annotation.NTNoteType type = (typeString != null) ? Annotation.NTNoteType.valueOf(typeString) : null;
			int id = data.getIntExtra(MainActivity.EXTRA_ID, -1);

//			Remove implementation
			NotesAdapter adapter = (NotesAdapter)((ListFragment) getFragmentManager().findFragmentById(R.id.f_list)).getAdapter();
			if (message == null && type == null && id >= 0) {
				adapter.removeItem(id);
				return;
			}

//    Create/Update functionality;
			Annotation annotation = (id == -1) ? new Note() : (Annotation) adapter.getItem(id);
			annotation.setMessage(message);
			annotation.setType(type);
			if (id < 0) {
				adapter.addItem(annotation);
			} else adapter.notifyDataSetChanged();
		}
	}


	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		ListFragment list = (ListFragment) getFragmentManager().findFragmentById(R.id.f_list);
		list.onSaveInstanceState(outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.ntlist, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_add_note) {
			Intent detailIntent = new Intent(this, DetailsActivity.class);
			startActivityForResult(detailIntent, NOTE_CREATE_REQUEST);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
