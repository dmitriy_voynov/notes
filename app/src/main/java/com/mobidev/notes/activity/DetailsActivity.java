package com.mobidev.notes.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Spinner;

import com.mobidev.notes.R;
import com.mobidev.notes.fragment.DetailFragment;

public class DetailsActivity extends Activity {

	private EditText mAnnotationMessage;
	private Spinner mTypeSpiner;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_ntdetail);
  }

	private void startingActivity(){
		DetailFragment fragment = (DetailFragment)getFragmentManager().findFragmentById(R.id.f_detail);

		Intent caller = getIntent();
		if (caller != null){
			String message = caller.getStringExtra(MainActivity.EXTRA_MESSAGE);
			if (message != null){
				fragment.setMessage(message);
			}
			String type = caller.getStringExtra(MainActivity.EXTRA_TYPE);
			if (type != null){
				fragment.setType(type);
			}
		}
	}

	protected  void onStart(){
		super.onStart();
		startingActivity();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		startingActivity();
	}

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.ntdetail, menu);
	  int id = getIntent().getIntExtra(MainActivity.EXTRA_ID,-1);
	  if (id == -1){
		  menu.findItem(R.id.activity_detail_action_delete).setVisible(false);
	  }
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
	  if (id == R.id.activity_detail_action_create){
		  Intent data = new Intent();
		  DetailFragment fragment = (DetailFragment)getFragmentManager().findFragmentById(R.id.f_detail);
		  String message = fragment.getMessage();
		  data.putExtra(MainActivity.EXTRA_MESSAGE, message);
		  String type = fragment.getType();
		  data.putExtra(MainActivity.EXTRA_TYPE,type);
		  data.putExtra(MainActivity.EXTRA_ID,getIntent().getIntExtra(MainActivity.EXTRA_ID,-1));
			setResult(RESULT_OK, data);
		  finish();
	  }
	  if (id == R.id.activity_detail_action_delete){
		  Intent data = new Intent();
		  data.putExtra(MainActivity.EXTRA_ID,getIntent().getIntExtra(MainActivity.EXTRA_ID,-1));
		  setResult(RESULT_OK, data);
		  finish();
	  }
    return super.onOptionsItemSelected(item);
  }
}
