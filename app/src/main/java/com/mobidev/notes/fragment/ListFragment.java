package com.mobidev.notes.fragment;

import android.os.Bundle;
import android.app.Fragment;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobidev.notes.R;

import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mobidev.notes.adapter.NotesAdapter;
import com.mobidev.notes.model.Annotation;

import java.util.ArrayList;


public class ListFragment extends Fragment {
	public final static String NOTES_STATE = "com.mobidev.NOTES";

	public interface ListCallbacks {
		public void onListItemClick(Annotation note, int id);
	}

	private ListCallbacks mCallbacks;
	private ListView mListView;

	public void setCallbacks(ListCallbacks callbacks) {
		mCallbacks = callbacks;
	}

	public Adapter getAdapter(){
		return mListView.getAdapter();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.f_list, container, false);
		mListView = (ListView) rootView.findViewById(R.id.NotesList);
		NotesAdapter adapter = new NotesAdapter(getActivity());
		if (savedInstanceState != null) {
			ArrayList<Parcelable> list = savedInstanceState.getParcelableArrayList(NOTES_STATE);
			if (list != null) {
				adapter.setItems(list);
			}
		}
		mListView.setAdapter(adapter);

		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView adapterView, View view, int i, long l) {
				if (mCallbacks != null) {
					Annotation annotation = (Annotation) adapterView.getAdapter().getItem(i);
					adapterView.setSelection(0);
					mCallbacks.onListItemClick(annotation, i);
				}
			}
		});
		return rootView;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		NotesAdapter adapter = (NotesAdapter) mListView.getAdapter();
		outState.putParcelableArrayList(NOTES_STATE, adapter.getItems());
	}

}
