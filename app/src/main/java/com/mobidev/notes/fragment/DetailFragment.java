package com.mobidev.notes.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.ArrayAdapter;

import com.mobidev.notes.R;
import com.mobidev.notes.model.Annotation;
import com.mobidev.notes.model.Note;

public class DetailFragment extends Fragment {
	public static DetailFragment newInstance(String param1, String param2) {
		DetailFragment fragment = new DetailFragment();
		return fragment;
	}

	private Spinner mTypeSpiner;
	private EditText mAnnotationMessage;

	public void setMessage(String message){
		mAnnotationMessage.setText(message);
	}

	public String getMessage(){
		return mAnnotationMessage.getText().toString();
	}
	public void setType(String type){
		mTypeSpiner.setSelection(Annotation.NTNoteType.valueOf(type).ordinal());
	}

	public String getType(){
		return ((Note.NTNoteType)mTypeSpiner.getSelectedItem()).name();
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.f_detail, container, false);
		mAnnotationMessage = (EditText)rootView.findViewById(R.id.activity_ntdetail_editText);
	  mTypeSpiner = (Spinner)rootView.findViewById(R.id.activity_ntdetail_type_spinner);
		mTypeSpiner.setAdapter(new ArrayAdapter<Annotation.NTNoteType>(getActivity(),R.layout.note_type_cell
				, Annotation.NTNoteType.values()));
		return rootView;
	}
}
