package com.mobidev.notes.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mobidev.notes.R;
import com.mobidev.notes.model.Annotation;
import com.mobidev.notes.model.Note;
import com.mobidev.notes.model.provider.NoteContract;

import java.util.ArrayList;
import java.util.List;

public class NotesAdapter extends BaseAdapter {

	// if we specify generic we can't return parcelable.
	private Context mContext;
	private LayoutInflater mLayoutInflater;

	public NotesAdapter(Context context){
		List<Annotation> notes = new ArrayList<Annotation>(1);
		notes.add(new Note("First note.", Annotation.NTNoteType.MEETING));
//		notes.add(new Note("Perform code review", Annotation.NTNoteType.CODEREVIEW));
//		notes.add(new Note("Check refactoring necessary for EBooks project", Annotation.NTNoteType.REFACTOR));
		init(context,notes);
	}

	public NotesAdapter(Context context, List items) {
		init(context,items);
	}

	static String URL = "content://com.mobidev.notes/notes";
	static Uri notes = Uri.parse(URL);

	private void init(Context context, List<Annotation> items){
		mContext = context;
//		addItem(items.get(0));
//		mLayoutInflater = LayoutInflater.from(context);
	}

	public void setItems(List items){/*mItems = items;*/}

	public ArrayList<Parcelable> getItems(){return null;/*return (ArrayList<Parcelable>) mItems;*/}


	public void addItem (Annotation note){
//		mItems.add(note);
		ContentValues values = new ContentValues();

		values.put(NoteContract.Columns.NOTE_MESSAGE, note.toString());
		values.put(NoteContract.Columns.NOTE_TYPE,note.typeToString());

		Uri uri = mContext.getContentResolver().insert(notes, values);
	}

	public void removeItem (Annotation note){
		String where = NoteContract.Columns.NOTE_MESSAGE + "=? AND " + NoteContract.Columns.NOTE_TYPE
											 + "=?";
		String [] args = {note.toString(),note.typeToString()};
		remove(where,args);
	}

	public void removeItem(int index){
		String where = "_ID=?";
		Integer lIndex = index;
		String [] args = {lIndex.toString()};
		remove(where,args);
	}

	private void remove(String where, String[] args){
		Uri notes = Uri.parse(URL);
		int count = mContext.getContentResolver().delete(notes,where, args);
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mContext.getContentResolver().query(notes, null, null, null, "noteId").getCount();
	}

	@Override
	public Object getItem(int i) {
		String where = "_ID=?";
		Integer lIndex = i;
		String [] args = {lIndex.toString()};
		Cursor mCursor = mContext.getContentResolver().query(notes, null, where, args, "noteId");
		if ( mCursor.moveToFirst()){
			String message = mCursor.getString(mCursor.getColumnIndex(NoteContract.Columns.NOTE_MESSAGE));
			String typeString = mCursor.getString(mCursor.getColumnIndex(NoteContract.Columns.NOTE_TYPE));
			Annotation.NTNoteType type = (typeString != null) ? Annotation.NTNoteType.valueOf(typeString) : null;
			return new Note(message, type);
		}
		return null;
//
////			Remove implementation
//		NotesAdapter adapter = (NotesAdapter)((ListFragment) getFragmentManager().findFragmentById(R.id.f_list)).getAdapter();
//		if (message == null && type == null && id >= 0) {
//			adapter.removeItem(id);
//			return;
//		}
//
////    Create/Update functionality;
//		Annotation annotation = (id == -1) ? new Note() : (Annotation) adapter.getItem(id);
//		annotation.setMessage(message);
//		annotation.setType(type);
//		if (id < 0) {
//			adapter.addItem(annotation);
//		} else adapter.notifyDataSetChanged();
//		result = result + "\n" + c.getString(c.getColumnIndex(BirthProvider.NAME)) +
//				71
//		" with id " +  c.getString(c.getColumnIndex(BirthProvider.ID)) +
//				72
//		" has birthday: " + c.getString(c.getColumnIndex(BirthProvider.BIRTHDAY));
	}

	@Override
	public long getItemId(int i) {
		return i;
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
		if (view == null) {
//			view = mLayoutInflater.inflate(R.layout.activity_ntlist_note_cell,viewGroup);
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.activity_ntlist_note_cell, null);
		}

		Annotation note = (Annotation)getItem(i);
		TextView annotationBody = (TextView)view.findViewById(R.id.textView);
		annotationBody.setText(note.toString());
		TextView annotationType = (TextView)view.findViewById(R.id.typeView);
		annotationType.setText(note.typeToString());
		return view;
	}
}
