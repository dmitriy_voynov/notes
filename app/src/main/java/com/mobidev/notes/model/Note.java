package com.mobidev.notes.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Note implements Annotation,Parcelable{

	private Annotation.NTNoteType mActivityType;
	private String mAnnotation;

	public Note(String annotation, NTNoteType activityType){
		initialize(annotation,activityType);
	}

	public Note() {
		mActivityType = null;
		mAnnotation = null;
	}

	private void initialize(String annotation, NTNoteType activityType){
		mActivityType = activityType;
		mAnnotation = annotation;
	}

	public String toString(){
		return mAnnotation;
	}
	public String typeToString() {
		return mActivityType.name();
	}
	public Annotation.NTNoteType getType(){
		return mActivityType;
	}
	public void setMessage(String note){mAnnotation = note;}
	public void setType(NTNoteType type){mActivityType = type;}

// Parcelable
	public int describeContents(){return 0;}

	public void writeToParcel(android.os.Parcel parcel, int i){
		parcel.writeString(mAnnotation);
		parcel.writeString(mActivityType.name());
	}

	public static final Parcelable.Creator<Note> CREATOR = new Creator<Note>() {
		@Override
		public Note createFromParcel(Parcel parcel) {
			return new Note(parcel);
		}

		@Override
		public Note[] newArray(int i) {
			return new Note[i];
		}
	};

	public Note(Parcel pc){
		mAnnotation = pc.readString();
		mActivityType =  Annotation.NTNoteType.valueOf(pc.readString());
	}
}
