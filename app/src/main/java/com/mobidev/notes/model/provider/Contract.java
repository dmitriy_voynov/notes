package com.mobidev.notes.model.provider;

import android.net.Uri;

public class Contract {
	public static final String AUTHORITY = "com.mobidev.notes";
	public static final Uri BASE_URI = Uri.parse(String.format("content://%s", AUTHORITY));

	public static final String VND_ANDROID_CURSOR_ITEM 	= "vnd.com.com.mobidev.notes.item/";
	public static final String VND_ANDROID_CURSOR_DIR 	= "vnd.com.com.mobidev.notes.dir/";


	public static final int FALSE 	= 0;
	public static final int TRUE 	= 1;

//	public static ArrayList<ContentProviderOperation> getOperationList(ContentProviderOperation ... operations ){
//		ArrayList<ContentProviderOperation> result = new ArrayList<ContentProviderOperation>();
//
//		for (ContentProviderOperation contentProviderOperation : operations) {
//			result.add(contentProviderOperation);
//		}
//
//		return result;
//	}
}
