package com.mobidev.notes.model.provider;

import android.content.ContentProviderOperation;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.provider.BaseColumns;


public class NoteContract extends Contract {


	public final static String ITEMS = "notes";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + ITEMS);

	public static final String DIR_MIME_TYPE = VND_ANDROID_CURSOR_ITEM + ITEMS;
	public static final String ITEM_MIME_TYPE = VND_ANDROID_CURSOR_DIR + ITEMS;

	public static final String[] PROJECTION = { Columns._ID, Columns.NOTE_ID, Columns.NOTE_TYPE,
			Columns.NOTE_MESSAGE};

	public interface Columns extends BaseColumns {
		public final static String NOTE_ID = "noteId";
		public final static String NOTE_TYPE = "type";
		public final static String NOTE_MESSAGE = "message";
	}

	public static ContentProviderOperation createInsertCollectionOperation(long id, String type, String message) {

//		PLog.v(12345, "createInsertCollectionOperation (%d)", userId);

		return ContentProviderOperation.newInsert(CONTENT_URI)
				.withValue(Columns.NOTE_ID, id)
				.withValue(Columns.NOTE_TYPE, type)
				.withValue(Columns.NOTE_MESSAGE, message).build();

	}

//	public static ContentProviderOperation createRenameOperation(long id, String name) {
//		return ContentProviderOperation.newUpdate(CONTENT_URI).withValue(Columns.COLLECTION_NAME, name)
//				.withSelection(Columns.NOTE_ID + "=?", new String[]{String.valueOf(id)}).build();
//
//	}

	public static CursorLoader queryAll(Context context) {
		return new CursorLoader(context, CONTENT_URI, PROJECTION, null, null, null);
	}

	public static CursorLoader queryById(Context context, long id) {
		return new CursorLoader(context, ContentUris.withAppendedId(CONTENT_URI, id), PROJECTION, null, null, null);
	}

//	public static CursorLoader queryByUserId(Context context, long id) {
//
//		String selection = Columns.USER_ID + " = " + id;
//	/*
//	 * "Defaultcollection" must be always hiden.
//	 */
//		selection += " AND " + Columns.COLLECTION_NAME + "!=" + DatabaseUtils.sqlEscapeString("Default Collection")
//				+ " COLLATE NOCASE";
//		return new CursorLoader(context,
//				CONTENT_URI,
//				PROJECTION,
//				selection,
//				null,
//				Columns.NOTE_ID + " DESC");
//	}

	public static ContentProviderOperation createDeleteOperation(long id) {
		return ContentProviderOperation.newDelete(CONTENT_URI)
				.withSelection(Columns.NOTE_ID + "=?", new String[]{String.valueOf(id)}).build();
	}

}
