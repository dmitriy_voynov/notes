package com.mobidev.notes.model.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class NotesSQLiteOpenHelper extends SQLiteOpenHelper{
	public NotesSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL("create table " + NoteContract.ITEMS	+ " (" +
						NoteContract.Columns._ID 					+ " INTEGER PRIMARY KEY AUTOINCREMENT, " +
						NoteContract.Columns.NOTE_ID 		+ " INTEGER UNIQUE, " +
						NoteContract.Columns.NOTE_TYPE 		+ " INTEGER, " +
						NoteContract.Columns.NOTE_MESSAGE	+ " TEXT, " +
						")"
		);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

}
