package com.mobidev.notes.model.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

public class NotesProvider extends ContentProvider{

	private final static int DB_VERSION = 1;
	private final static String DB_NAME = "notes_db";

	private static final UriMatcher URI_MATCHER;

	private static final int NOTE_DIR 	= 100;
	private static final int NOTE_ITEM 	= 101;


	static {

		URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

		URI_MATCHER.addURI(Contract.AUTHORITY, NoteContract.ITEMS, NOTE_DIR);
		URI_MATCHER.addURI(Contract.AUTHORITY, NoteContract.ITEMS + "/#", NOTE_ITEM);
	}

	private NotesSQLiteOpenHelper mSQLHelper;

	private String addSelectionById(String selection, String id){
		if (TextUtils.isEmpty(selection)){
			return BaseColumns._ID + " = " + id;
		}else{
			return selection + BaseColumns._ID + " = " + id;
		}
	}

	@Override
	public boolean onCreate() {
		mSQLHelper = new NotesSQLiteOpenHelper(getContext(), DB_NAME, null, DB_VERSION);
		return false;
	}

	@Override
	public String getType(Uri uri) {

		final String result;

		switch (URI_MATCHER.match(uri)) {
			case NOTE_DIR:
				result = NoteContract.DIR_MIME_TYPE;
				break;
			case NOTE_ITEM:
				result = NoteContract.ITEM_MIME_TYPE;
				break;

			default:
				throw new IllegalArgumentException("Unsuported URI:" + uri);
		}

		return result;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		final String tableName;
		switch (URI_MATCHER.match(uri)) {
			case NOTE_DIR: {
				tableName = NoteContract.ITEMS;
			}
			break;
		default:
			throw new IllegalArgumentException("Unsuported URI:" + uri);
		}

		final long id = mSQLHelper.getWritableDatabase().replace(tableName, null, values);
		final Uri newUri = ContentUris.withAppendedId(uri, id);
		getContext().getContentResolver().notifyChange(newUri, null);
		return newUri;

	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

		SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

		switch (URI_MATCHER.match(uri)) {
			case NOTE_ITEM:
				selection = addSelectionById(selection, uri.getLastPathSegment());
			case NOTE_DIR: {
				builder.setTables(NoteContract.ITEMS);
			}
			break;
			default:
				throw new IllegalArgumentException("Unsuported URI:" + uri);
		}

		Cursor cursor = builder.query(mSQLHelper.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);

		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

		final String tableName;

		switch (URI_MATCHER.match(uri)) {
			case NOTE_ITEM:
				selection = addSelectionById(selection, uri.getLastPathSegment());
			case NOTE_DIR: {
				tableName = NoteContract.ITEMS;
			}
			break;
			default:
				throw new IllegalArgumentException("Unsuported URI:" + uri);
		}

		int count = mSQLHelper.getWritableDatabase().update(tableName, values, selection, selectionArgs);
		getContext().getContentResolver().notifyChange(uri, null);

		return count;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		final String tableName;

		switch (URI_MATCHER.match(uri)) {
			case NOTE_ITEM:
				selection = addSelectionById(selection, uri.getLastPathSegment());
			case NOTE_DIR: {
				tableName = NoteContract.ITEMS;
			}
			break;
			default:
				throw new IllegalArgumentException("Unsuported URI:" + uri);
		}

		int count = mSQLHelper.getWritableDatabase().delete(tableName, selection, selectionArgs);
		getContext().getContentResolver().notifyChange(uri, null);

		return count;
	}


}
