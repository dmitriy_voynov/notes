package com.mobidev.notes.model;

public interface Annotation{
	public enum NTNoteType{
		MEETING,CODEREVIEW,REFACTOR
	}

	@Override
	public java.lang.String toString();

	public java.lang.String typeToString();
	public NTNoteType getType();

	public void setMessage(String note);
	public void setType(NTNoteType type);
}
